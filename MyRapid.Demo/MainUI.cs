/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.Diagram.Core.Localization;
using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.Utils.Localization;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraTabbedMdi;
using MyRapid.Base.Page;
using MyRapid.Code;
using MyRapid.GlobalLocalizer;
using MyRapid.Proxy;
using MyRapid.Proxy.MainService;
using MyRapid.SysEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using System.Drawing;
using DevExpress.UserSkins;
using DevExpress.Skins;
namespace MyRapid.Demo
{
    public partial class MainUI : MdiForm
    {
        #region 初始化

        /// <summary>
        /// 当前用户
        /// </summary>
        private Aut_User sys_User;

        /// <summary>
        /// 当前语言定位器
        /// </summary>
        private MyCommonLocalizer myCommonLocalizer = new MyCommonLocalizer();

        public MainUI()
        {
            BonusSkins.Register();
            SkinManager.EnableFormSkins();
            //加载缓存 多国语言  历史登录信息 等
            InitializeLocalCache();
            #region 用户尝试登录系统
            //暂停计时器 登陆过程不计时
            Shared.Timer.Stop();
            //尝试登陆
            LoginBox login = new LoginBox();
            login.ShowDialog();
            sys_User = Provider.SysUser;
            if (sys_User == null)
                Environment.Exit(0);
            //重新启动计时器
            Shared.Timer.Start();

            //加载图标 加载菜单、按钮、审核操作的图标 
            InitializeImage();
            #endregion
            #region 加载系统主界面
            try
            {
                #region InitializeComponent
                //进度条
                SplashScreenManager.ShowForm(this, typeof(WaitForm), false, false, false);
                //记载字体 皮肤
                InitializeAppearance();
                SendSpalsh(10, myCommonLocalizer.GetLocalizedString(MyCommonStringId.Initialize_GlobalLocalizer));
                //加载多国语言
                InitializeGlobalLocalizer();
                SendSpalsh(20, myCommonLocalizer.GetLocalizedString(MyCommonStringId.Initialize_Component));
                InitializeComponent();
                SkinHelper.InitSkinGalleryDropDown(mySkin);
                barUserNick.Caption = sys_User.User_Nick;
                SendSpalsh(50, myCommonLocalizer.GetLocalizedString(MyCommonStringId.Initialize_CommonLocalizer));
                InitializeCommonLocalizer();
                SendSpalsh(60, myCommonLocalizer.GetLocalizedString(MyCommonStringId.Initialize_Image));
                //InitializeImage();
                SendSpalsh(80, myCommonLocalizer.GetLocalizedString(MyCommonStringId.Initialize_Menu));
                InitializeMenu();
                SendSpalsh(90, myCommonLocalizer.GetLocalizedString(MyCommonStringId.Initialize_Configuration));
                //InitializeConfiguration();
                //SendSpalsh(95, myCommonLocalizer.GetLocalizedString(MyCommonStringId.Initialize_AutoUpdate));
                //InitializeAutoUpdate();
                InitializeMessage();
                SendSpalsh(100, myCommonLocalizer.GetLocalizedString(MyCommonStringId.Initialize_Finish));



                #endregion
            }
            catch (Exception ex)
            {
                Shared.ShowErr(ex);
                Process.GetCurrentProcess().Kill();
                //Application.Exit();
                //Environment.Exit(0);
            }
            #endregion
        }

        #endregion

        #region 窗体控制 语言 闪屏
        /// <summary>
        /// 加载窗体控件语言
        /// </summary>
        private void InitializeCommonLocalizer()
        {
            ThreadHelper.Start(() =>
            {
                this.Text = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MainUI_Text);
                barMenu.Hint = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_ShowHideMenuHint);
                barMenu.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_ShowHideMenu);
                barHelp.Hint = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_HelpHint);
                barHelp.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_Help);
                barUserNick.Hint = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_UserNickHint);
                barMenuNick.Hint = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_MenuNickHint);
                barAuther.Hint = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_AuthorHint);
                barVersion.Hint = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_VersionHint);
                barMenuNick.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_MenuNickHint);
                barAuther.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_AuthorHint);
                barVersion.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_VersionHint);
                barSkin.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_UserSkin);
                barSkin.Hint = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_UserSkinHint);

                barUser.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_UserInfo);
                barUser.Hint = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MenuCmd_UserInfoHint);

                barCloseThis.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.CloseMenu_This);
                barCloseOther.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.CloseMenu_Other);
                barCloseLeft.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.CloseMenu_Left);
                barCloseRight.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.CloseMenu_Right);
                barCloseAll.Caption = myCommonLocalizer.GetLocalizedString(MyCommonStringId.CloseMenu_All);
                Provider.Set("MainUI_Error", myCommonLocalizer.GetLocalizedString(MyCommonStringId.MainUI_Error));
                Provider.Set("ChildPage_Error", myCommonLocalizer.GetLocalizedString(MyCommonStringId.ChildPage_Error));
                Provider.Set("ChildPage_ConfirmCaption", myCommonLocalizer.GetLocalizedString(MyCommonStringId.ChildPage_ConfirmCaption));
                Provider.Set("ChildPage_ConfirmText", myCommonLocalizer.GetLocalizedString(MyCommonStringId.ChildPage_ConfirmText));
            });

        }

        /// <summary>
        /// 向进度窗口发命令
        /// </summary>
        /// <param name="Int"></param>
        /// <param name="Text"></param>
        private void SendSpalsh(int Int, string Text)
        {
            MyWaitInfo myWaitInfo = new MyWaitInfo();
            myWaitInfo.ProgressInt = Int;
            myWaitInfo.ProgressText = Text;
            SplashScreenManager.Default.SendCommand(WaitForm.SplashScreenCommand.Setinfo, myWaitInfo);

        }


        /// <summary>
        /// 窗体加载结束
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainUI_Shown(object sender, EventArgs e)
        {

            try
            {

                SplashScreenManager.CloseForm(false);

                //如果没有页面编辑权限则隐藏编辑按钮
                if (!Shared.HasMenu("ea03d32b-cd93-4ccc-b6f8-ffc846330c12"))
                    barEdit.Visibility = BarItemVisibility.Never;

                Provider.Set("BarTip", barTip);
                //呈现账套名称
                string multiName = Provider.Get("Multi").ToStringEx();
                if (!string.IsNullOrEmpty(multiName))
                {
                    barMulti.Caption = multiName;
                    barMulti.Visibility = BarItemVisibility.Always;
                }
            }
            catch (Exception ex)
            {
                Shared.ShowErr(ex);
            }
            finally
            {
                barTip.Caption = Shared.Timer.Diff();
            }

        }

        private void MainUI_Load(object sender, EventArgs e)
        {
            try
            {
                MyTree.SelectImageList = (ImageCollection)Provider.Get("SmallIconList");
                MyTree.ImageIndexFieldName = "Menu_IconIndex";
                myMdi.Images = (ImageCollection)Provider.Get("SmallIconList");
                MyTree.DataSource = Provider.UserMenus;
                bool IsFavorite = CacheHelper.Get<bool>("IsFavorite");
                if (IsFavorite)
                {
                    barFavio.PerformClick();
                }
            }
            catch (Exception ex)
            {
                Shared.ShowErr(ex);
            }

        }

        private Sys_Menu CurMenu;
        private void MyMdi_SelectedPageChanged(object sender, EventArgs e)
        {

            try
            {
                GC.Collect();
                if (myMdi.SelectedPage != null)
                {
                    //Read_Sys_Help_ForMenu
                    SubForm myPage = (SubForm)myMdi.SelectedPage.MdiChild;


                    Sys_Menu sys_Menu = myPage.SysMenu;
                    Provider.SysMenu = sys_Menu;
                    Provider.CurrForm = myPage;
                    if (sys_Menu != null)
                    {
                        barMenuNick.Caption = sys_Menu.Menu_Nick;
                        string mEx = "{0}\r\n{1}";
                        if (!string.IsNullOrEmpty(sys_Menu.Menu_File) && !string.IsNullOrEmpty(sys_Menu.Menu_Class))
                        {
                            barMenuNick.Hint = string.Format(mEx, sys_Menu.Menu_File, sys_Menu.Menu_Class);
                        }
                        else
                        {
                            barMenuNick.Hint = null;
                        }

                        barVersion.Caption = sys_Menu.Menu_Version;
                        barVersion.Hint = myPage.ProductVersion;
                        if (myPage.ProductVersion != sys_Menu.Menu_Version)
                        {
                            barVersion.ItemAppearance.Normal.Options.UseBackColor = true;
                            barVersion.ItemAppearance.Normal.BackColor = Color.Red;
                        }
                        else
                        {
                            barVersion.ItemAppearance.Normal.Options.UseBackColor = false;
                        }
                        //barVersion.Hint = sys_Menu.Menu_Class;
                        barAuther.Caption = sys_Menu.Menu_Author;
                        //barAuther.Hint = sys_Menu.Menu_File;
                        myMdi.SelectedPage.ImageIndex = 0;
                        CurMenu = sys_Menu;
                        myMdi.SelectedPage.ImageIndex = (int)sys_Menu.Menu_IconIndex;
                        barMenuNick.Caption = sys_Menu.Menu_Nick;

                    }
                }
                else
                {
                    barMenuNick.Caption = "页面名称";
                    barVersion.Caption = "页面版本";
                    barAuther.Caption = "页面作者";
                    barMenuNick.Hint = string.Empty;
                    barVersion.Hint = string.Empty;
                    barAuther.Hint = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Shared.ShowErr(ex);
            }

        }

        private void MainUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            CacheHelper.Save();
            if (Provider.SysUser == null)
                return;
            string exitDetail = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MainUI_Exit);
            string exitText = myCommonLocalizer.GetLocalizedString(MyCommonStringId.MainUI_ExitDetail);
            if (XtraMessageBox.Show(exitText, exitDetail, MessageBoxButtons.YesNo) != DialogResult.Yes)
            {
                e.Cancel = true;
                return;
            }


        }
        #endregion

        #region 页签右键菜单
        private void MyMdi_MouseUp(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Right && ActiveMdiChild != null)
            {
                DevExpress.XtraTab.ViewInfo.BaseTabHitInfo hInfo = myMdi.CalcHitInfo(e.Location);
                //右键点击位置：在Page上且不在关闭按钮内
                if (hInfo.IsValid && hInfo.Page != null && !hInfo.InPageControlBox)
                {
                    this.closeMenu.ShowPopup(Control.MousePosition);//在鼠标位置弹出，而不是e.Location
                }
            }

        }
        //关闭当前
        private void barCloseThis_ItemClick(object sender, ItemClickEventArgs e)
        {

            this.myMdi.SelectedPage.MdiChild.Close();
            //this.ActiveMdiChild.Close();

        }
        //关闭其他
        private void barCloseOther_ItemClick(object sender, ItemClickEventArgs e)
        {

            XtraMdiTabPage xmp = this.myMdi.SelectedPage;
            while (this.myMdi.Pages[0] != xmp)
            {
                this.myMdi.Pages[0].MdiChild.Close();
            }
            while (this.myMdi.Pages[this.myMdi.Pages.Count - 1] != xmp)
            {
                this.myMdi.Pages[this.myMdi.Pages.Count - 1].MdiChild.Close();
            }

        }
        //关闭左边
        private void barCloseLeft_ItemClick(object sender, ItemClickEventArgs e)
        {

            XtraMdiTabPage xmp = this.myMdi.SelectedPage;
            while (this.myMdi.Pages[0] != xmp)
            {
                this.myMdi.Pages[0].MdiChild.Close();
            }

        }
        //关闭右边
        private void barCloseRight_ItemClick(object sender, ItemClickEventArgs e)
        {

            XtraMdiTabPage xmp = this.myMdi.SelectedPage;
            while (this.myMdi.Pages[this.myMdi.Pages.Count - 1] != xmp)
            {
                this.myMdi.Pages[this.myMdi.Pages.Count - 1].MdiChild.Close();
            }

        }
        //关闭所有
        private void barCloseAll_ItemClick(object sender, ItemClickEventArgs e)
        {

            while (this.myMdi.Pages.Count > 0)
            {
                this.myMdi.Pages[0].MdiChild.Close();
            }

        }
        #endregion

        #region 右侧按钮

        private void barEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = Shared.LoadMenu("ea03d32b-cd93-4ccc-b6f8-ffc846330c12");
            if (frm != null)
            {
                Shared.ShowMenu(frm, this);
            }
        }

        /// <summary>
        /// 刷新当前登录用户
        /// </summary>
        private void MainUI_Activated(object sender, EventArgs e)
        {
            if (BaseService.IsFaulted)
            {
                Provider.SysUser = null;
            }
            sys_User = Provider.SysUser;
            if (sys_User != null)
            {
                barUserNick.Caption = Provider.SysUser.User_Nick;
            }
            else
            {
                Shared.TryLogin();
            }
            if (Provider.SysUser == null)
            {
                Process.GetCurrentProcess().Kill();
            }
            sys_User = (Aut_User)Provider.SysUser;

        }

        private void MainUI_BackColorChanged(object sender, EventArgs e)
        {
            try
            {
                //SaveSkin to Database
                //RaiseError(myCommonLocalizer.GetLocalizedString(MyCommonStringId.MainUI_Exit_Succeed), myCommonLocalizer.GetLocalizedString(MyCommonStringId.MainUI_Exit_SucceedDetail));
                sys_User.User_Skin = UserLookAndFeel.Default.ActiveSkinName;
                List<MyParameter> mps = new List<MyParameter>();
                mps.Add("@User_Skin", DbType.String, sys_User.User_Skin, null);
                BaseService.ExecuteAsync("SystemUser_Skin", mps);
            }
            catch //(Exception ex)
            {
                //SharedFunc.RaiseError(ex);
            }
            try
            {
                //SaveSkin to Cache
                List<DynamicEntity> LoginInfos = CacheHelper.Get<List<DynamicEntity>>("SysLogin");
                dynamic loginInfo = LoginInfos.Find(l => ((dynamic)l).UserName.ToUpper().Equals(sys_User.User_Name.ToUpper()));
                if (loginInfo != null)
                {
                    loginInfo.SkinName = UserLookAndFeel.Default.ActiveSkinName;
                    CacheHelper.Set("SysLogin", LoginInfos);
                }
            }
            catch //(Exception ex)
            {
                //SharedFunc.RaiseError(ex);
            }
        }

        private void barUser_ItemClick(object sender, ItemClickEventArgs e)
        {
            UserBox userInfo = new UserBox();
            userInfo.ShowDialog();
        }
        RichBox richPage = null;
        private void barHelp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (CurMenu == null) return;
            if (richPage == null) richPage = new RichBox();
            richPage.StartPosition = FormStartPosition.Manual;
            richPage.TopMost = true;
            if (this.WindowState == FormWindowState.Maximized)
            {
                richPage.Top = this.ClientRectangle.Top;
                richPage.Height = this.ClientRectangle.Height;
                richPage.Left = this.ClientRectangle.Left + this.ClientRectangle.Width - richPage.Width;
            }
            else
            {
                richPage.Top = this.Top;
                richPage.Height = this.Height;
                richPage.Left = this.Left + this.Width;
            }
            //只有开发部的人可以修改帮助文档
            richPage.ReadOnly = sys_User.User_Department != "66d7e954-abe0-42ce-898a-0351e042448a";
            richPage.RtfText = CurMenu.Menu_Help;
            richPage.FormClosing += (s, ev) =>
             {
                 //文本有变化
                 if (richPage.CanUndo && richPage.DialogResult == DialogResult.OK)
                 {
                     string helpRtf = richPage.RtfText;
                     ThreadHelper.Start(() =>
                     {
                         try
                         {
                             CurMenu.Menu_Help = helpRtf;
                             List<MyParameter> mps = new List<MyParameter>();
                             mps.Add("@Menu_Help", DbType.String, CurMenu.Menu_Help, null);
                             mps.Add(Constant.Parameter_MenuId, DbType.String, CurMenu.Menu_Id, null);
                             BaseService.Execute(Constant.SystemHelp_Menu, mps, "U");
                         }
                         catch (Exception ex)
                         {
                             Shared.ShowErr(ex);
                         }

                     });

                 }
                 ev.Cancel = true;
                 richPage.Hide();
             };

            richPage.Show();

        }

        #endregion

        #region 菜单面板
        //展开收合菜单
        private void MyLeftPanel_DoubleClick(object sender, EventArgs e)
        {
            if (!MyTree.Nodes.FirstNode.Expanded)
                MyTree.ExpandAll();
            else
                MyTree.CollapseAll();
        }

        //双击打开页面
        private void MyTree_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Shared.Timer.Restart();
                List<Sys_Menu> MenuData = (List<Sys_Menu>)Provider.UserMenus;
                if (MyTree.FocusedNode == null) return;
                Sys_Menu sys_Menu = (Sys_Menu)MyTree.GetFocusedRow();
                if (sys_Menu == null) return;
                //目录不做操作
                if (sys_Menu.Menu_Show == "3") return;
                //按住Control键就在新窗口打开
                if (Control.ModifierKeys == Keys.Control)
                    sys_Menu.Menu_Show = "2";

                //取得图标索引
                ImageCollection SmallIconList = (ImageCollection)Provider.Get("SmallIconList");
                if (SmallIconList.Images.Keys.Contains(sys_Menu.Menu_Icon))
                    sys_Menu.Menu_IconIndex = SmallIconList.Images.Keys.IndexOf(sys_Menu.Menu_Icon);

                ImageCollection ims = (ImageCollection)MyTree.SelectImageList;
                if (ims != null)
                    barTip.ImageOptions.Image = ims.Images[sys_Menu.Menu_IconIndex];// MyTree.FocusedNode.ImageIndex sys_Menu.Menu_IconIndex;
                SubForm myPage = Shared.LoadMenu(sys_Menu);
                Shared.ShowMenu(myPage, this);
                barTip.Caption = Shared.Timer.Diff();
                Shared.Timer.Start();
                barTip.Hint = string.Empty;
            }
            catch (Exception ex)
            {
                Shared.ShowErr(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        //显示 或 隐藏 菜单
        private void barMenu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {
                MyLeftPanel.Visible = !MyLeftPanel.Visible;
                splitterControl1.Visible = !splitterControl1.Visible;
                if (MyLeftPanel.Visible)
                {
                    InitializeMenu();
                    MyTree.DataSource = Provider.UserMenus;
                }
            }
            catch (Exception ex)
            {
                Shared.ShowErr(ex);
            }
            finally
            {
            }

        }

        #endregion

        #region 收藏夹
        // // CREATE TABLE [dbo].[Aut_Favorite](
        // //  [Favorite_Id] [nvarchar] (50) NOT NULL,
        // //  [Favorite_Name] [nvarchar] (50) NULL,
        // //	[Favorite_Nick] [nvarchar] (50) NULL,
        // //	[Favorite_Parent] [nvarchar] (50) NULL,
        // //	[Favorite_Menu] [nvarchar] (50) NULL,
        // //	[Favorite_User] [nvarchar] (50) NULL,
        // //	[IsEnabled] [bit] NULL,
        // //	[IsDeleted] [bit] NULL,
        // //	[Remark] [nvarchar] (max) NULL,
        // //  [Creator] [nvarchar] (50) NULL,
        // //	[CreateTime] [datetime] NULL,
        // //	[Modifier] [nvarchar] (50) NULL,
        // //	[ModifyTime] [datetime] NULL,
        // //	[Deleter] [nvarchar] (50) NULL,
        // //	[DeleteTime] [datetime] NULL,
        // //	[RowVersion] [timestamp] NOT NULL,
        // // CONSTRAINT[PK_Aut_Favorite_d8a6b50b_35d5_40c1_afcc_6b74662430df] PRIMARY KEY CLUSTERED
        // //(
        // //   [Favorite_Id] ASC
        // //)WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]
        // //) ON[PRIMARY] TEXTIMAGE_ON[PRIMARY]
        // //GO
        // //EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Aut_Favorite', @level2type = N'COLUMN', @level2name = N'Favorite_Id'
        // //GO
        // //EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'代码' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Aut_Favorite', @level2type = N'COLUMN', @level2name = N'Favorite_Name'
        // //GO
        // //EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'名称' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Aut_Favorite', @level2type = N'COLUMN', @level2name = N'Favorite_Nick'
        // //GO
        // //EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'分组' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Aut_Favorite', @level2type = N'COLUMN', @level2name = N'Favorite_Parent'
        // //GO
        // //EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'菜单' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Aut_Favorite', @level2type = N'COLUMN', @level2name = N'Favorite_Menu'
        // //GO
        // //EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Aut_Favorite', @level2type = N'COLUMN', @level2name = N'Favorite_User'
        // //GO
        // //EXEC sys.sp_addextendedproperty @name = N'EX_Description', @value = N'11 用户角色' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Aut_Favorite'
        // //GO
        // //EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户菜单' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Aut_Favorite'
        // //GO

        // private void myFavi_BeforePopup(object sender, System.ComponentModel.CancelEventArgs e)
        // {
        //     myFaviSub.ClearLinks();

        //     List<Sys_Menu> MenuData = (List<Sys_Menu>)Provider.UserMenus;
        //     if (MyTree.FocusedNode == null) return;
        //     string mId = (string)MyTree.FocusedNode.GetValue("Menu_Id");
        //     Sys_Menu CurMenu = MenuData.Find(m => m.Menu_Id == mId);
        //     if (CurMenu == null) return;
        //     List<MyParameter> mps = new List<MyParameter>();
        //     mps.Add(Constant.Parameter_MenuId, DbType.String, CurMenu.Menu_Id, null);
        //     DataTable dt = BaseService.GetData("SystemMenu_Pavi", mps);
        //     foreach (DataRow dr in dt.Rows)
        //     {
        //         BarButtonItem barItem = new BarButtonItem();
        //         barItem.Caption = (string)dr["Favorite_Nick"];
        //         barItem.Tag = dr["Favorite_Menu"];
        //         myFaviSub.AddItem(barItem);
        //         barItem.ItemClick += delegate
        //         {
        //             mps = new List<MyParameter>();
        //             mps.Add(Constant.Parameter_MenuId, DbType.String, CurMenu.Menu_Id, null);
        //             //Favorite_Parent
        //             mps.Add("@Favorite_Parent", DbType.String, barItem.Tag, null);
        //             mps.Add("@Favorite_Name", DbType.String, CurMenu.Menu_Name, null);
        //             mps.Add("@Favorite_Nick", DbType.String, CurMenu.Menu_Nick, null);
        //             BaseService.Execute(Constant.SystemMenu_Favi, mps, "U");
        //         };
        //     }

        //     BarButtonItem barNew = new BarButtonItem();
        //     barNew.Caption = "新的分组";
        //     barNew.Tag = Guid.NewGuid().ToString();
        //     myFaviSub.AddItem(barNew);
        //     barNew.ItemClick += delegate
        //     {
        //         InputBox ib = new InputBox();
        //         if (ib.ShowDialog() == DialogResult.OK)
        //         {
        //             mps = new List<MyParameter>();
        //             mps.Add(Constant.Parameter_MenuId, DbType.String, barNew.Tag, null);
        //             //Favorite_Parent
        //             mps.Add("@Favorite_Parent", DbType.String, "", null);
        //             mps.Add("@Favorite_Name", DbType.String, ib.EditValue, null);
        //             mps.Add("@Favorite_Nick", DbType.String, ib.EditValue, null);
        //             BaseService.Execute(Constant.SystemMenu_Favi, mps, "U");
        //         }

        //     };
        // }

        // ////添加收藏
        // //private void barAdd_ItemClick(object sender, ItemClickEventArgs e)
        // //{

        // //}

        // //删除收藏
        // private void barRemove_ItemClick(object sender, ItemClickEventArgs e)
        // {
        //     if (MyTree.FocusedNode == null) return;
        //     string mId = (string)MyTree.FocusedNode.GetValue("Menu_Id");
        //     if (string.IsNullOrEmpty(mId)) return;
        //     List<MyParameter> mps = new List<MyParameter>();
        //     mps.Add(Constant.Parameter_MenuId, DbType.String, mId, null);
        //     BaseService.Execute(Constant.SystemMenu_Favi, mps, "D");
        // }
        // //清空收藏
        // private void barClear_ItemClick(object sender, ItemClickEventArgs e)
        // {
        //     BaseService.Execute("SystemMenu_Pavi", null, "D");
        // }

        // //所有菜单
        // private void barAll_ItemClick(object sender, ItemClickEventArgs e)
        // {
        //     CacheHelper.Set("IsFavorite", false);
        //     InitializeMenu();
        //     MyTree.DataSource = Provider.UserMenus;
        // }
        // //收藏菜单
        // private void barFavio_ItemClick(object sender, ItemClickEventArgs e)
        // {
        //     CacheHelper.Set("IsFavorite", true);
        //     ImageCollection SmallIconList = (ImageCollection)Provider.Get("SmallIconList");
        //     List<Sys_Menu> MenuData;
        //     DataTable dt = BaseService.GetData(Constant.SystemMenu_Favi, null);
        //     MenuData = EntityHelper.GetEntities<Sys_Menu>(dt);
        //     foreach (Sys_Menu sys_Menu in MenuData)
        //     {
        //         if (SmallIconList.Images.Keys.Contains(sys_Menu.Menu_Icon))
        //             sys_Menu.Menu_IconIndex = SmallIconList.Images.Keys.IndexOf(sys_Menu.Menu_Icon);
        //     }
        //     MyTree.DataSource = MenuData;

        // }

        #endregion

        #region 远程协助
        private void barAssist_ItemClick(object sender, ItemClickEventArgs e)
        {
            Process da = new Process();
            string sharePath = Application.StartupPath + @"\Library\ShareDesktop.exe";
            string tokenPath = Application.StartupPath + @"\Library\token.txt";
            da.StartInfo.FileName = sharePath;
            da.StartInfo.WorkingDirectory = Path.GetDirectoryName(sharePath);
            string arg = "server";
            da.StartInfo.Arguments = arg;
            da.Start();
            string token = "";
            Stopwatch sw = Shared.Timer.Restart();
            ThreadHelper.Start(() =>
            {
                while (string.IsNullOrEmpty(token))
                {

                    if (File.Exists(tokenPath))
                    {
                        token = File.ReadAllText(tokenPath);
                        System.Threading.Thread.Sleep(1000);
                        File.Delete(tokenPath);
                    }
                    if (sw.Elapsed.Seconds > 30)
                        return;
                }
                List<MyParameter> mps = new List<MyParameter>();
                mps.Add("@User_Assist", DbType.String, token, null);
                BaseService.ExecuteAsync("SystemUser_Assist", mps);
                Console.WriteLine(token);
            });

        }


        #endregion

        #region 沟通

        string defTalk = "";
        private void InitializeMessage()
        {
            defTalk = BaseService.GetValue("MSGTalk_New", null).ToStringEx();
            if (!string.IsNullOrEmpty(defTalk))
                Shared.Twinkle(barTalk, true);
        }

        Talk.TalkUI talk = null;
        private void barTalk_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (talk == null || talk.IsDisposed)
            {
                talk = new MyRapid.Talk.TalkUI(defTalk);
                talk.TalkItem = barTalk;
            }
            talk.ShowInTaskbar = false;
            talk.Show();
            talk.Activate();
        }
        #endregion


    }
}