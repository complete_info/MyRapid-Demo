﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using MyRapid.Base.Page;

namespace MyRapid.Demo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Shared.Timer.Start();
            InitializeException();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("zh-Hans");
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("zh-Hans");
            BonusSkins.Register();
            SkinManager.EnableFormSkins();

            //UserLookAndFeel.Default.SetSkinStyle("DevExpress Style");
            //Application.UseWaitCursor = true;
            Application.Run(new MainUI());
        }

        #region InitializeException
        private static void InitializeException()
        {
            //处理未捕获的异常   
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            //处理UI线程异常   
            Application.ThreadException += Application_ThreadException;
            //处理非UI线程异常   
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception error = e.ExceptionObject as Exception;
            if (error != null)
            {
                Shared.ShowErr(error);
                //BaseService.SaveLog(error.Message, error.StackTrace, (byte)Sys_Log_Type.Fatal, string.Empty, "UnhandledException");
            }
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Shared.ShowErr(e.Exception);
        }
        #endregion
    }
}
