/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using MyRapid.Code;
using MyRapid.GlobalLocalizer;
using MyRapid.Proxy;
using MyRapid.Proxy.MainService;
using MyRapid.SysEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using MyRapid.Base.Page;

namespace MyRapid.Demo
{
    public partial class LoginBox : XtraForm
    {
        private string PassMask = "@@@@@@";
        private List<DynamicEntity> LoginInfos = new List<DynamicEntity>();
        private MyCommonLocalizer myCommonLocalizer = new MyCommonLocalizer();
        public LoginBox()
        {
            InitializeComponent();
            ContextMenu emptyMenu = new ContextMenu();
            txtUsername.Properties.ContextMenu = emptyMenu;
            txtPassword.Properties.ContextMenu = emptyMenu;
            //pictureEdit1.Properties.ContextMenu = emptyMenu;
            //layoutControl1.ContextMenu = emptyMenu;
            txtUsername.Properties.Items.Clear();
            LoginInfos = CacheHelper.Get<List<DynamicEntity>>("SysLogin");
            if (LoginInfos != null)
                LoginInfos = LoginInfos.Distinct().ToList();
            if (LoginInfos == null) LoginInfos = new List<DynamicEntity>();
            foreach (dynamic config in LoginInfos.OrderByDescending(l => ((dynamic)l).LastLogin))
            {
                txtUsername.Properties.Items.Add(config.UserName);
            }
            txtUsername.SelectedIndex = 0;
            string mainText = myCommonLocalizer.GetLocalizedString(MyCommonStringId.Login_Text);
            if (!string.IsNullOrEmpty(mainText)) this.Text = mainText;
            string btnLoginText = myCommonLocalizer.GetLocalizedString(MyCommonStringId.Login_Button_Login);
            if (!string.IsNullOrEmpty(btnLoginText)) btnLogin.Text = btnLoginText;
            string btnCancelText = myCommonLocalizer.GetLocalizedString(MyCommonStringId.Login_Button_Cancel);
            if (!string.IsNullOrEmpty(btnCancelText)) btnCancel.Text = btnCancelText;
            string lytUsernameText = myCommonLocalizer.GetLocalizedString(MyCommonStringId.Login_UserName);
            if (!string.IsNullOrEmpty(lytUsernameText)) lytUsername.Text = lytUsernameText;
            string lytPasswordText = myCommonLocalizer.GetLocalizedString(MyCommonStringId.Login_Password);
            if (!string.IsNullOrEmpty(lytPasswordText)) lytPassword.Text = lytPasswordText;
            string lytMultiText = myCommonLocalizer.GetLocalizedString(MyCommonStringId.Login_Multi);
            if (!string.IsNullOrEmpty(lytPasswordText)) lytMulti.Text = lytPasswordText;
            string chkUserNameText = myCommonLocalizer.GetLocalizedString(MyCommonStringId.Login_Check_UserName);
            if (!string.IsNullOrEmpty(chkUserNameText)) chkUserName.Text = chkUserNameText;
            string chkPasswordText = myCommonLocalizer.GetLocalizedString(MyCommonStringId.Login_Check_Password);
            if (!string.IsNullOrEmpty(chkPasswordText)) chkPassword.Text = chkPasswordText;

            if (Configuration.GetValue("hasMulti", "False").ToLower() == "true")
            {
                DataTable dt = BaseService.GetMulti();
                txtMulti.Properties.DataSource = dt;
                if (dt.Rows.Count > 0)
                    txtMulti.ItemIndex = 0;
                if (dt.Rows.Count < 2)
                    lytMulti.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

                dynamic loginInfo = LoginInfos.Find(l => ((dynamic)l).UserName == txtUsername.Text);
                txtMulti.EditValue = loginInfo.Multi;
            }
            else
            {
                lytMulti.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private string EncryptPassword;
        private void txtUsername_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                dynamic loginInfo = LoginInfos.Find(l => ((dynamic)l).UserName == txtUsername.Text);
                if (loginInfo != null)
                {
                    chkUserName.Checked = loginInfo.RemUserName;
                    chkPassword.Checked = loginInfo.RemPassword;
                    txtMulti.EditValue = loginInfo.Multi;
                    if (loginInfo.RemPassword)
                    {
                        EncryptPassword = Encrypt.TokenEncrypt(Encrypt.TokenDecrypt(loginInfo.Password));
                        txtPassword.Text = PassMask;
                        chkUserName.Checked = true;
                    }
                    UserLookAndFeel.Default.SetSkinStyle(loginInfo.SkinName);
                    //DevExpress.Utils.AppearanceObject.DefaultFont = new System.Drawing.Font(loginInfo..Font ,);
                    //DevExpress.XtraEditors.WindowsFormsSettings.DefaultFont = new System.Drawing.Font("微软雅黑", 9);
                }
            }
            catch (Exception ex)
            {
                Shared.ShowErr(ex);
            }

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                EncryptPassword = Encrypt.TokenEncrypt(Encrypt.TokenDecrypt(EncryptPassword));
                string json = BaseService.Login(txtUsername.Text, EncryptPassword, txtMulti.EditValue.ToStringEx());
                Aut_User sys_User = json.ToObject<Aut_User>();
                if (sys_User == null || sys_User.User_Password == null)
                {
                    string err = myCommonLocalizer.GetLocalizedString(MyCommonStringId.Login_Error);
                    string errDetail = myCommonLocalizer.GetLocalizedString(MyCommonStringId.Login_Error_Detail);
                    //BaseService.SaveLog(err, errDetail, (byte)Sys_Log_Type.Information, string.Empty, this.Name + ":" + txtUsername.Text);
                    Shared.ShowErr(err, errDetail);
                    return;
                }
                Provider.SysUser = sys_User;
                dynamic loginInfo = LoginInfos.Find(l => ((dynamic)l).UserName == txtUsername.Text);
                if (loginInfo == null)
                {
                    loginInfo = new DynamicEntity();
                    LoginInfos.Add(loginInfo);
                }
                loginInfo.UserName = txtUsername.Text;
                loginInfo.RemUserName = chkUserName.Checked;
                loginInfo.RemPassword = chkPassword.Checked;
                loginInfo.Multi = (string)txtMulti.EditValue;
                loginInfo.Password = string.Empty;
                loginInfo.LastLogin = DateTime.Now;
                loginInfo.SkinName = sys_User.User_Skin;
                if (loginInfo.RemPassword)
                {
                    loginInfo.Password = EncryptPassword;
                }
                CacheHelper.Set("SysLogin", LoginInfos);
                this.Close();
            }
            catch (Exception ex)
            {
                Shared.ShowErr(ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPassword_EditValueChanged(object sender, EventArgs e)
        {
            if (txtPassword.Text.Equals(PassMask)) return;
            EncryptPassword = Encrypt.TokenEncrypt(Encrypt.MD5Encrypt(txtPassword.Text));
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            txtPassword.Text = string.Empty;
        }

        private void txtUsername_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Close)
            {
                txtUsername.Properties.Items.Remove(txtUsername.Text);
                LoginInfos.RemoveAll(l => ((dynamic)l).UserName == txtUsername.Text);
                if (txtUsername.Properties.Items.Count > 0)
                {
                    txtUsername.SelectedIndex = 0;
                }
                else
                {
                    txtUsername.Text = "";
                    txtPassword.Text = "";
                }
            }
        }

        private void txtMulti_EditValueChanged(object sender, EventArgs e)
        {
            Provider.Set("Multi", txtMulti.Text);
        }

        //加载布局配置
        //string fileXML = "LoginBox.xml";
        private void LoginBox_Load(object sender, EventArgs e)
        {
            //if (File.Exists(fileXML))
            //{
            //    layoutControl1.RestoreLayoutFromXml(fileXML);
            //    this.Location = CacheHelper.Get<Point>("LoginBoxLocation");
            //    this.Size = CacheHelper.Get<Size>("LoginBoxSize");
            //    pictureEdit1.Image = CacheHelper.Get<string>("LoginBoxLogo").ToImage();
            //}
        }

        private void LoginBox_FormClosing(object sender, FormClosingEventArgs e)
        {
            //CacheHelper.Set("LoginBoxSize", this.Size);
            //CacheHelper.Set("LoginBoxLocation", this.Location);
            //CacheHelper.Set("LoginBoxLogo", pictureEdit1.Image.ToBase64());
            //CacheHelper.Save();
            //layoutControl1.SaveLayoutToXml(fileXML);

        }


    }

}